'use strict';

/*
*
    Usage:
    CLI `gulp dev` for browsersync pointed at a node http-server
    on port:8080
 
    This gulpfile relies on promises as a workaround for gulp3.x's lack
    of async cooperation between tasks
 
* /
*/

// require gulp & friends
let gulp = require('gulp'),
    gutil = require('gulp-util'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    prefix = require('gulp-autoprefixer'),
    inlinesource = require('gulp-inline-source'),
    htmlmin = require('gulp-htmlmin'),
    sourcemaps = require('gulp-sourcemaps'),
    uncss = require('gulp-uncss'),
    asyncTimeout = 2000,
    browserSync = require('browser-sync').create();


let prodStyles = () => {
    return new Promise( (resolve, reject) => {
      setTimeout(() => {
        gulp.src('./src/scss/app.scss')
            .pipe(sass({
                outputStyle: 'compressed'
            }).on('error', sass.logError))
            .pipe(uncss({
                html: ['./src/index.html']
            }))
            .pipe(prefix())
            // name output file
            .pipe(gulp.dest('./dist/'));
        resolve();
      }, asyncTimeout * 2);
    });
};

let inlineSourceTask = () => {
    return new Promise( (resolve, reject) => {
      setTimeout(() => {
        gulp.src('./src/*.html')
            .pipe(inlinesource())
            .pipe(gulp.dest('./'));
        resolve();
      }, asyncTimeout);
    });
};

let minHtmlTask = () => {
    return new Promise( (resolve, reject) => {
        setTimeout(() => {
            gulp.src('./*.html')
                .pipe(htmlmin({collapseWhitespace: true}))
                .pipe(gulp.dest('./'));
            resolve();
        }, asyncTimeout);
    });
};

gulp.task('inline-n-min', () => {
    prodStyles().then(() => {  
        inlineSourceTask().then(() => {
            minHtmlTask();
        });
    });
});

gulp.task('dev-styles', () => {
    gulp.src('./src/scss/app.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(prefix())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist/'))
        .pipe(browserSync.stream());
});

gulp.task('dev-html', () => {
    gulp.src('./src/*.html')
        .pipe(gulp.dest('./'));
});

// watch scss and scripts
gulp.task('watch', () => {
    gulp.watch('./src/**/*.scss', ['dev-styles']);
    gulp.watch('./src/**/*.html', ['dev-html']);
    gulp.watch(['./index.html']).on('change', browserSync.reload);
    browserSync.init({
        proxy: 'localhost:8080',
        open: false
    });
});

// dev tasks
gulp.task('dev', ['dev-styles', 'dev-html', 'watch']);
// production tasks
gulp.task('default', ['inline-n-min']);
